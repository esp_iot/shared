#!/bin/bash

# Initialize default values for SERVER_URL and SERVER_PORT
SERVER_URL="http://localhost"
SERVER_PORT="3000"

CURRENT_TIME=$(date +%s)

# Function to display usage
usage() {
    echo "Usage: $0 [-U SERVER_URL] [-p SERVER_PORT] [-d OAUTH_URI -c OAUTH_CLIENTID> -u OAUTH_USERNAME> -P OAUTH_PASSWORD] <key> <hash> <file_path>"
    echo "  -u SERVER_URL       Optional. The URL of the server. Default is http://localhost."
    echo "  -p SERVER_PORT      Optional. The server port. Default is 3000."
    echo "  -d OAUTH_URI        Optional. OAUTH2 Discovery URI, leave blank to send without authentication. Default is without authentication"
    echo "  -c OAUTH_CLIENTID   Optional. OAUTH2 Client_ID. Required if [-d OAUTH_URI] is provided."
    echo "  -U OAUTH_USERNAME   Optional. OAUTH2 Username. Required if [-d OAUTH_URI] is provided."
    echo "  -P OAUTH_PASSWORD   Optional. OAUTH2 Password. Required if [-d OAUTH_URI] is provided."
    echo "  <key>               Required. The key associated with the file."
    echo "  <hash>              Required. The hash of the file."
    echo "  <file_path>         Required. The path to the file to be uploaded."
    exit 1
}

# Load environment variables if .env exists
if [ -f .env ]; then
  export $(grep -v '^#' .env | xargs)
fi

# Parse optional arguments
while getopts ":u:p:d:c:U:P:" opt; do
  case ${opt} in
    u )
      SERVER_URL=$(echo $OPTARG | tr -d '\r')
      ;;
    p )
      SERVER_PORT=$(echo $OPTARG | tr -d '\r')
      ;;
    d )
      OAUTH_URI=$(echo $OPTARG | tr -d '\r')
      ;;
    c )
      OAUTH_CLIENTID=$(echo $OPTARG | tr -d '\r')
      ;;
    U )
      OAUTH_USERNAME=$(echo $OPTARG | tr -d '\r')
      ;;
    P )
      OAUTH_PASSWORD=$(echo $OPTARG | tr -d '\r')
      ;;
    \? )
      usage
      ;;
  esac
done
# Remove line feeds (\r)
SERVER_URL=$(echo $SERVER_URL | tr -d '\r')
SERVER_PORT=$(echo $SERVER_PORT | tr -d '\r')
OAUTH_URI=$(echo $OAUTH_URI | tr -d '\r')
OAUTH_CLIENTID=$(echo $OAUTH_CLIENTID | tr -d '\r')
OAUTH_USERNAME=$(echo $OAUTH_USERNAME | tr -d '\r')
OAUTH_PASSWORD=$(echo $OAUTH_PASSWORD | tr -d '\r')

shift $((OPTIND -1))

# Check for required positional arguments
if [ "$#" -ne 3 ]; then
    usage
fi

KEY=$1
HASH=$2
FILE_PATH=$3

# Use environment variables if set, overriding defaults and optional arguments
SERVER_URL=${SERVER_URL:-$SERVER_URL}
SERVER_PORT=${SERVER_PORT:-$SERVER_PORT}
OAUTH_URI=${OAUTH_URI:-$OAUTH_URI}
OAUTH_CLIENTID=${OAUTH_CLIENTID:-$OAUTH_CLIENTID}
OAUTH_USERNAME=${OAUTH_USERNAME:-$OAUTH_USERNAME}
OAUTH_PASSWORD=${OAUTH_PASSWORD:-$OAUTH_PASSWORD}

# Construct the URL
URL="${SERVER_URL}:${SERVER_PORT}/commit/${KEY}/${HASH}"

if [ -z "$OAUTH_URI" ]; then
  # Send request WITHOUT authentication
  # Use curl to send a PUT request with the file
  curl -X PUT -F "file=@${FILE_PATH}" ${URL}
else
  # Send request WITH authentication
  if [ -z "$OAUTH_CLIENTID" ] || [ -z "$OAUTH_USERNAME" ] || [ -z "$OAUTH_PASSWORD" ]; then
    echo "Error: Missing required OAUTH arguments ('-c', '-U', '-P') for '-d' option."
    usage
    exit 1
  fi

  # Fetch the discovery document
  DISCOVERY_DOC=$(curl -s $OAUTH_URI)
  if [ -z "$DISCOVERY_DOC" ]; then
    echo "Error: Failed to get Discovery Document. Check your OAUTH_URI!"
    exit 1
  fi

  # Get the token_endpoint
  TOKEN_URI=$(echo $DISCOVERY_DOC | jq -r '.token_endpoint')
  if [ -z "$TOKEN_URI" ]; then
    echo "Error: Failed to get Token URI. Check that 'jq' is installed!"
    exit 1
  fi

  # TODO: Check existing token if valid
  # Request a new token using secrets and save the response to a file
  TOKEN_RESPONSE_FILE=$(mktemp)
  curl -s -X POST "$TOKEN_URI" \
      -H 'Content-Type: application/x-www-form-urlencoded' \
      -d "grant_type=client_credentials&client_id=$OAUTH_CLIENTID&username=$OAUTH_USERNAME&password=$OAUTH_PASSWORD" \
      -o "$TOKEN_RESPONSE_FILE"

  # Parse response
  OAUTH_TOKEN=$(jq -r '.access_token' "$TOKEN_RESPONSE_FILE")
  EXPIRES_IN=$(jq -r '.expires_in' "$TOKEN_RESPONSE_FILE")
  EXPIRES_AT=$((CURRENT_TIME + EXPIRES_IN))

  # TODO: Store these for reuse (if still valid)

  # Use curl to send a PUT request with the file
  curl -X PUT -H "Authorization:Bearer $OAUTH_TOKEN" -F "file=@${FILE_PATH}" ${URL}
fi

echo # Print a newline for cleaner output
