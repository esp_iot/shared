# ESP_IOT - Shared

Repository of scripts and other files for the ESP_IOT project


## `commit_file.sh`

The commit_file.sh script facilitates uploading files to the [firmware_server](https://gitlab.com/esp_iot/firmware_server). 

### Usage

Basic help is available with the `-h` argument
```bash
./commit_file.sh -h
```
At minimum `commit_file.sh` requires three arguments: the project_key, hash, and path to the file you wish to upload. There are additional arguments for changing the server address and port (default is `localhost:3000`), as well as supporting OAUTH2.0 authenticated servers (set with `REQUIRE_AUTH=true`)

```bash
./commit_file.sh <project_key> <hash> <file_path>
```

For example:

```bash
./commit_file.sh my_project 0123456 /path/to/file.bin
```

Use the `-u` and `-p` arguments to specify a different firmware_server location / port

```bash
./commit_file.sh -u <SERVER_ADDR> -p <SERVER_PORT> <project_key> <hash> <file_path>
```

For example:

```bash
./commit_file.sh -U https://fw.example.com -p 443 my_project 0123456 /path/to/file.bin
```

The commit_file.sh script can automatically request an OAUTH2.0 token and use it to authenticate the commit request.

```bash
./commit_file.sh -d <OAUTH_URI> -c <CLIENT_ID> -U <USERNAME> -P <PASSWORD> <project_key> <hash> <file_path>
```

For example:

```bash
./commit_file.sh -d https://auth.example.com/.well-known/openid-configuration -c aBcDeF -U ESP_IOT -P passw0rd my_project 0123456 /path/to/file.bin
```

### Tips


- Ensure you make the script executable before running it:

```bash
chmod +x commit_file.sh
```


- Many of these options can also be provided in a `.env` file located next to `commit_file.sh`. For example:

```bash
SERVER_URL=https://fw.example.com
SERVER_PORT=443

# Discovery URI of your OAUTH2.0 Authentication Server
OAUTH_URI=https://auth.example.com/.well-known/openid-configuration

# OAUTH2.0 Client information (used to request a valid token)
OAUTH_CLIENTID=aBcDeF
OAUTH_USERNAME=ESP_IOT
OAUTH_PASSWORD=passw0rd
```

## Contributing
We welcome contributions! Please open an issue or submit a pull request for any improvements or feature additions.